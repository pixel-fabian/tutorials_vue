export type Character = {
  id: number
  name: string
  killcount: number
  faction: string
}
