import { defineStore } from 'pinia'
import type { User } from '@/globalTypes'

type State = {
  users: User[]
  user: User
}

export const useUserStore = defineStore('UserStore', {
  state: (): State => ({
    users: [],
    user: {}
  }),
  getters: {},
  actions: {
    async updateUsers() {
      const response = await fetch('https://jsonplaceholder.typicode.com/users')
      this.users = await response.json()
    },
    async updateUser(id: string) {
      const response = await fetch(`https://jsonplaceholder.typicode.com/users/${id}`)
      this.user = await response.json()
    }
  }
})
