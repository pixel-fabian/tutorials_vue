# vue 3 fundamentals

## getting started

### CDN

```javascript
<script src="https://unpkg.com/vue@3/dist/vue.global.js"></script>

<div id="app">{{ message }}</div>

<script>
  const { createApp } = Vue

  const app = createApp({
    data() {
      return {
        message: 'Hello Vue!'
      }
    }
  });

  app.mount('#app')
</script>
```

### create app

```javascript
npm init vue@latest
```
