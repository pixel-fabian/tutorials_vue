# Vue.js Tutorials

Learning about Vue.js from:

1. [Tyler Potts: Build a QUIZ app using Vue JS](https://invidious.privacydev.net/watch?v=6cXWWOxrZiw)
2. [Traversy Media: Full Stack Vue.js, Express & MongoDB](https://invidious.privacydev.net/playlist?list=PLillGF-RfqbYSx-Ab1xWVanGKtowTsnNm)
3. [Frontendmasters: Vue 3 Fundamentals](https://frontendmasters.com/courses/vue-fundamentals/)
4. [Frontendmasters: Building Applications with Vue & Nuxt](https://frontendmasters.com/courses/vue-nuxt-apps/)
5. [The Net Ninja: Vue.js 3 Tutorial](https://invidious.privacydev.net/playlist?list=PL4cUxeGkcC9hYYGbV60Vq3IXYNfDk8At1)
6. [The Net Ninja: Pinia Crash Course](https://invidious.privacydev.net/playlist?list=PL4cUxeGkcC9hp28dYyYBy3xoOdoeNw-hD)
7. [The Net Ninja: Nuxt 3 Tutorial](https://invidious.privacydev.net/playlist?list=PL4cUxeGkcC9haQlqdCQyYmL_27TesCGPC)
8. [Frontendmasters: Nuxt 3 Fundamentals](https://frontendmasters.com/courses/nuxt/)
9. [Nuxt Layers](https://frontendmasters.com/courses/nuxt/)
10. [VueMastery](https://www.vuemastery.com)

## Links

- [vuejs.org](https://vuejs.org/guide/quick-start.html)
- [pinia.vuejs.org](https://pinia.vuejs.org/) store for Vue.js
- [Nuxt](https://nuxt.com/)
- [unjs](https://unjs.io/)

## Libraries

useful libraries:

- [vuetifyjs.com](https://vuetifyjs.com/en/) Component Library
- [VueUse](https://vueuse.org/) Composition Utilities
