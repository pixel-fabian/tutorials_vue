import { describe, it, expect } from 'vitest'
import { mount } from '@vue/test-utils'
import NotificationToast from './NotificationToast.vue'

describe('NotificationToast', () => {
  it('has the correct class for error', () => {
    const status = 'error'
    const message = 'Text of the notification'
    const wrapper = mount(NotificationToast, {
      props: { status, message }
    })
    expect(wrapper.classes()).toEqual(expect.arrayContaining(['notification--error']))
  })

  it('has the correct class for success', () => {
    const status = 'success'
    const message = 'Text of the notification'
    const wrapper = mount(NotificationToast, {
      props: { status, message }
    })
    expect(wrapper.classes()).toEqual(expect.arrayContaining(['notification--success']))
  })

  it('has the correct class for info', () => {
    const status = 'info'
    const message = 'Text of the notification'
    const wrapper = mount(NotificationToast, {
      props: { status, message }
    })
    expect(wrapper.classes()).toEqual(expect.arrayContaining(['notification--info']))
  })

  it('slides up, when message is empty', () => {
    const status = 'info'
    const message = ''
    const wrapper = mount(NotificationToast, {
      props: { status, message }
    })
    expect(wrapper.classes('notification--slide')).toBe(false)
  })

  it('emits event when close button is pressed', async () => {
    const status = 'info'
    const message = 'Text of the notification'
    const wrapper = mount(NotificationToast, {
      props: { status, message },
      data() {
        return {
          clicked: false
        }
      }
    })
    const closeButton = wrapper.find('button')
    await closeButton.trigger('click')
    expect(wrapper.emitted()).toHaveProperty('clear-notification')
  })

  it('renders message', () => {
    const status = 'info'
    const message = 'Text of the notification'
    const wrapper = mount(NotificationToast, {
      props: { status, message }
    })
    expect(wrapper.find('p').text()).toBe(message)
  })
})
