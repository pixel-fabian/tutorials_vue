import type { Task } from "@/types";
import { defineStore } from "pinia";

/**
 * Store to handle tasks
 */
export const useTaskStore = defineStore("taskStore", {
  state: () => ({
    tasks: [] as Task[],
    isLoading: false,
  }),
  getters: {
    favs(): Task[] {
      return this.tasks.filter((task) => task.isFav);
    },
  },
  actions: {
    async getTasks() {
      this.isLoading = true;
      const res = await fetch("http://localhost:3000/tasks");
      const data = await res.json();
      this.tasks = data;
      this.isLoading = false;
    },
    async addTask(task: Task) {
      const res = await fetch("http://localhost:3000/tasks", {
        method: "POST",
        body: JSON.stringify(task),
        headers: { "Content-Type": "application/json" },
      });
      if (!res.ok) {
        console.error(res.statusText);
      } else {
        this.tasks.push(task);
      }
    },
    generateId(): number {
      if (this.tasks.length > 0) {
        const lastId = this.tasks[this.tasks.length - 1].id;
        return lastId + 1;
      } else {
        return 1;
      }
    },
    async deleteTask(id: number) {
      const res = await fetch(`http://localhost:3000/tasks/${id}`, {
        method: "DELETE",
      });
      if (!res.ok) {
        console.error(res.statusText);
      } else {
        this.tasks = this.tasks.filter((task) => {
          return task.id != id;
        });
      }
    },
    async toggleFav(id: number) {
      const task = this.tasks.find((task) => task.id === id);
      if (task) {
        const res = await fetch(`http://localhost:3000/tasks/${id}`, {
          method: "PATCH",
          body: JSON.stringify({ isFav: !task.isFav }),
          headers: { "Content-Type": "application/json" },
        });
        if (!res.ok) {
          console.error(res.statusText);
        } else {
          task.isFav = !task.isFav;
        }
      }
    },
  },
});
