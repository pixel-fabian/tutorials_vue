const app = Vue.createApp({
  data() {
    return {
      showButtons: false,
      age: 45,
      x: 0,
      y: 0,
      books: [
        {
          title: "The Final Empire",
          author: "Brandon Sanderson",
          age: 45,
          img: "assets/finalempire.jpg",
          isFav: true,
        },
        {
          title: "The Way of Kings",
          author: "Brandon Sanderson",
          age: 45,
          img: "assets/thewayofthekings.jpg",
          isFav: false,
        },
        {
          title: "Name of the Wind",
          author: "Patrick Rothfuss",
          age: 52,
          img: "assets/nameofthewind.jpg",
          isFav: false,
        },
      ],
      url: "https://fabian-kurz.de",
    };
  },
  methods: {
    changeTitle(title) {
      this.books[0].title = title;
    },
    toggleButtons() {
      this.showButtons = !this.showButtons;
    },
    toogleFav(book) {
      book.isFav = !book.isFav;
    },
    handleEvent(event, data) {
      console.log(event.type);
      if (data) {
        console.log(data);
      }
    },
    handleMousemove(event) {
      this.x = event.offsetX;
      this.y = event.offsetY;
    },
  },
  computed: {
    filteredBooks() {
      return this.books.filter((book) => book.isFav);
    },
  },
});

app.mount("#app");
