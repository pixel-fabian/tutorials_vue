import { ref } from "vue";

export default (id: number) => {
  const post = ref({});
  const errorMsg = ref("");

  const loadPost = async () => {
    try {
      const response = await fetch(`http://localhost:3000/posts/${id}`);
      if (!response.ok) {
        throw Error("Fetching data failed");
      }
      post.value = await response.json();
    } catch (err: any) {
      errorMsg.value = err.message;
    }
  };

  return { post, errorMsg, loadPost };
};
