import { ref } from "vue";

export default () => {
  const posts = ref([]);
  const errorMsg = ref("");

  const loadPosts = async () => {
    try {
      const response = await fetch("http://localhost:3000/posts");
      if (!response.ok) {
        throw Error("Fetching data failed");
      }
      posts.value = await response.json();
    } catch (err: any) {
      errorMsg.value = err.message;
    }
  };

  return { posts, errorMsg, loadPosts };
};
